/*!
 * @author juan.urgilesc <juan.urgilesc@gmail.com>
 * @author Diego Molina <degomolina@gmail.com>
 * date 10/22/2020
 * Firmador NODEJS Servicio de Rentas Internas Ecuador XADES-BES
 */

const Sign = require("./sign");
const Sri = require("./sri")

module.exports = {Sri,Sign};