"use strict";
/*!
 * @author juan.urgilesc <juan.urgilesc@gmail.com>
 * @author Diego Molina <degomolina@gmail.com>
 * date 10/22/2020
 * Firmador NODEJS Servicio de Rentas Internas Ecuador XADES-BES
 */

/**
 * Dependencias
 */
const forge = require("node-forge");
const moment = require("moment-timezone");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const fs = require("fs");

/**
 * Método para la firma Xades con firmaXadesBes.jar
 * @method firmarJava 
 * @param {string} xmlString XML to sign
 * @param {string} key certificado .p12
 * @param {string} pass Base64 contraseña
 * @returns {String} xmlString firmado
 * @public
 */
(async () => {
  exports.firmarJava = async (documentoXML, certificado, contrasena) => {
    try {
      let xmlString = await leerArchivo(documentoXML);
      let buf1 = Buffer.from(certificado);
      let buf = Buffer.from(contrasena);
      let keyb = buf1.toString("base64");
      let passb = buf.toString("base64");

      const { stdout, stderr } = await exec(
        "java -jar ./node_modules/sri-doc/firmaXadesBes.jar '" +
        xmlString +
        "' " +
        keyb +
        " " +
        passb
      );
      return stdout;
    } catch (err) {
      throw err;
    }
  };

  //funcion para leer el archivo desde su path
  function leerArchivo(docu) {
    return new Promise(function (resolve, reject) {
      fs.readFile(docu, 'utf8', function (err, data) {
        if (err) {
          reject(err);
          return;
        }
        resolve(data);
      });
    });
  }
})();

/**
 * Método para la verificación de la caducidad de la firma (Solo en local)
 *
 * @param {string} key certificado .p12
 * @param {string} pass Base64 contraseña
 *
 */
exports.verifySignature = async (llavecriptografica, pass) => {
  var key = fs.readFileSync(llavecriptografica, "binary");
  try {
    const p12base64 = key;
    const asn = forge.asn1.fromDer(p12base64);
    const p12 = forge.pkcs12.pkcs12FromAsn1(asn, true, pass);
    const certBags = p12.getBags({ bagType: forge.pki.oids.certBag })[
      forge.pki.oids.certBag
    ];
    const expiresOn = certBags[0].cert.validity.notAfter;
    const date = moment().format();
    let timediff = moment(expiresOn).diff(date, "seconds");
    if (timediff < 100000) {
      //cert is expired or expires in one day
      return {
        isValid: false,
        message: "Firma expirada.",
      };
    }
    return {
      isValid: true,
      expiresOn,
    };
  } catch (err) {
    return {
      isValid: false,
      message: "Error en la llave criptográfica y clave de la misma.",
    };
  }
};
/**
 * Método para la verificación de la caducidad de la firma (Solo en local)
 *
 * @param {string} key certificado .p12
 * @param {string} pass Base64 contraseña
 *
 */
exports.verifySignatureFile = async (key, pass) => {
  
  try {
    const p12base64 = key;
    const asn = forge.asn1.fromDer(p12base64);
    const p12 = forge.pkcs12.pkcs12FromAsn1(asn, true, pass);
    const certBags = p12.getBags({ bagType: forge.pki.oids.certBag })[
      forge.pki.oids.certBag
    ];
    const expiresOn = certBags[0].cert.validity.notAfter;
    const date = moment().format();
    let timediff = moment(expiresOn).diff(date, "seconds");
    if (timediff < 100000) {
      //cert is expired or expires in one day
      return {
        isValid: false,
        message: "Firma expirada.",
      };
    }
    return {
      isValid: true,
      expiresOn,
      message: "Llave válida.",
    };
  } catch (err) {
    return {
      isValid: false,
      message: "Error en la llave criptográfica y clave de la misma.",
    };
    // console.log(err);
    // throw new Error(
    //   "Error en la llave criptográfica y clave de la misma. Verificar la información en https://www.gob.ec/articulos/firmaec"
    // );
  }
};