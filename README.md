# sri-doc

Módulo para nodejs firma y envio de documentos sri

# Realizado
- Pruebas actuales clase firma para realizar pruebas de generación de facturas y firma
- Pruebas generando factura
- Pruebas firma factura


# Por realizar
- validacion de p12
- limpieza y organizacion Orientada a objetos
- envio a servicio de pruebas
- crear clases utiles

# Instalar
Para instalar las librerias
```console
    $ npm install --save https://gitlab.com/katarisoft/sri-doc.git
```

# Ejemplo para consumir la librería del SRI  
 ```javascript
/**
* Ejemplo de como consumir la librería sir-doc KATARISOTF
*/
let sridoc = require('sri-doc')
var fs = require("fs");
let Sri = sridoc.Sri
let Sign = sridoc.Sign;

//Path del certificado P12 y contraseña
const certificado = "XXXXXXXXXXX.p12";
const contrasena = "XXXXXXXXX";
//documento xml para pruebas con la estructura solicitada por el SRI
const documento = "/home/diego/Descargas/prueba.xml";

//variables para apuntar a los webServices de Pruebas o Producción del SRI
const wsdlUrl = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl";
const wsdlUrlAutorizacion = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl";
//variable para cargar las URL de los web services del SRI ya sea pruebas o producción en la librería sri-docs
let sri = new Sri(wsdlUrl, wsdlUrlAutorizacion);

//Verificar la firma si es valida y fecha de caducidad en local
Sign.verifySignature(certificado, contrasena).then(respuesta => {
    try {
        console.log(JSON.stringify(respuesta))
    } catch (error) {
        throw error;
    }
});

//Envio del documento a la librería para su firma
Sign.firmarJava(documento, certificado, contrasena).then(respuesta => {
    try {
        //Guardar el archivo firmado en una carpeta
        fs.writeFileSync("/home/diego/Descargas/uno.xml", respuesta);
        console.log("Documento Firmado")
    } catch (error) {
        throw error;
    }
});

//Enviar documentos para la aprobación de la estructura del XML obtenemos la clave de acceso
sri.revisarDocsSRI("/home/diego/Descargas/uno.xml").then(respuesta => {
    try {
        console.log(JSON.stringify(respuesta))
    } catch (error) {
        throw error;
    }
});

//Enviar Clave de acceso para la autorización.
sri.aprobarDocsSRI("65465465465465465465").then(respuesta => {
    try {
        console.log(JSON.stringify(respuesta))
    } catch (error) {
        throw error;
    }
});

 ```

 # Respuesta Revisión

 Clave ya registrada
```javascript
{
   "RespuestaRecepcionComprobante": {
     "estado": "DEVUELTA",
     "comprobantes": {
       "comprobante": {
         "claveAcceso": "2210202001010127545100110010010000000200000001013",
         "mensajes": {
           "mensaje": {
             "identificador": "43",
             "mensaje": "CLAVE ACCESO REGISTRADA",
             "tipo": "ERROR"
           }
         }
       }
     }
   }
 }
 ```

 # Respuestas Autorizacion

 Xml Autorizado
 ```javascript
    {
    "RespuestaAutorizacionComprobante":{
        "claveAccesoConsultada":"2210202001010127545100110010010000000200000001013",
        "numeroComprobantes":"1",
        "autorizaciones":{
            "autorizacion":{
                "estado":"AUTORIZADO",
                "numeroAutorizacion":"2210202001010127545100110010010000000200000001013",
                "fechaAutorizacion":"2020-10-23T02:11:47.000Z",
                "ambiente":"PRUEBAS",
    "comprobante":"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><factura id=\"comprobante\" version=\"1.0.0\"> 
    ...
    </factura>",
                "mensajes":null
            }
        }
    }
    }
```

Errores 
```javascript
    {
    "RespuestaAutorizacionComprobante": {
    "claveAccesoConsultada": "22102020010101275451001100100100000002000000010131",
        "autorizaciones": {
        "autorizacion": {
            "estado": "RECHAZADA",
            "mensajes": {
            "mensaje": {
                "identificador": "80",
                "mensaje": "ERROR EN LA ESTRUCTURA DE LA CLAVE DE ACCESO",
    "informacionAdicional": "El Secuencial  000000021 contenido en la clave de acceso no corresponde al de la etiqueta 000000024",
        "tipo": "ERROR"
    
            }
            }
        }
        }
    }
    }
```