const soap = require("soap");
const { Buffer } = require("buffer");
var fs = require("fs"); //file system

//urls para verificar y aprobar la estructura del xml en el sri ambiente de pruebas
/*!
 * @author juan.urgilesc <juan.urgilesc@gmail.com>
 * @author Diego Molina <dego.molina@gmail.com>
 * date 10/22/2020
 * Sri servicio par aenvio y recuperación servicios Soap SRI
 */
class Sri {
  //Enviar documentos para revisión de estructura
  wsdlUrl;
  wsdlUrlAutorizacion;

  /**
   * constructor,
   * @param {String} wsdlUrlRevision  `default` Url de revisión servicios web Sri
   * @param {String} wsdlAutorizacion  `default` Url de autorización servicios web Sri
   */
  constructor(wsdlUrlRevision, wsdlAutorizacion) {
    this.wsdlUrl = wsdlUrlRevision;
    this.wsdlUrlAutorizacion = wsdlAutorizacion;
    this.respuestaSRI = {};
    this.respuestaAutorizacionSRI = {};
    this.respuestaSRI = {};
    this.respuestaAutorizacionSRI = {};
  };

  /**
   * RevisarDocsSRI Método para envío a revisión a los servicios web del Servicio de Rentas Internas,
   * @param {String} documentoFirmado  `default` Documento xml para envío a revisión
   *
   */
  async revisarDocsSRI(documentoFirmado) {
    let readFile = fs.readFileSync(documentoFirmado, 'utf8')
    var aux = new Buffer.from(readFile).toString("base64");
    let documentoEnvio = { xml: aux };
    //Enviamos Factura Firmada al SRI actualmente utilizamos FacturaUno para pruebas despues cambiar por la variable de factura_firmada
    return new Promise((resolve, reject) => {
      soap.createClient(this.wsdlUrl, function (err, client) {
        if (err) {
          return reject("Servicio WEB del SRI No disponible")
        }
        client.validarComprobante(documentoEnvio, function (err, result) {
          if (err) {
            return reject("Servicio WEB del SRI No disponible")
          }
          let respuestaSRI = {};
          respuestaSRI = result;
          return resolve(respuestaSRI);
        });
      });
    });
  }

  /**
   * RevisarDocsSRI Método para envío a autorización a los servicios web del Servicio de Rentas Internas,
   * @param {String} clavAcceso  `default` Clave de acceso para la autorización
   *
   */
  aprobarDocsSRI(clavAcceso) {
    let claveEnvio = { claveAccesoComprobante: clavAcceso };
    return new Promise((resolve, reject) => {
      soap.createClient(this.wsdlUrlAutorizacion, function (err, client) {
        if (err) {
          return reject("Servicio WEB del SRI No disponible")
        }
        client.autorizacionComprobante(claveEnvio, function (error, data) {
          if (err) {
            return reject("Servicio WEB del SRI No disponible")
          }
          let respuestaAutorizacionSRI = {};
          respuestaAutorizacionSRI = data;
          return resolve(data);
        });
      });
    });
  }
}

module.exports = Sri;